# Helm chart repo

This is the hosting server for Privacy1 helm charts repo. The repo currently includes the following charts

* Harpocrates

The build pipeline will build all charts and expose them into the helm chart repository with the following URL

```
https://privacy1.gitlab.io/helm-chart-repo
```


# How to create sha for chart tar file manually

```
$ openssl dgst -sha256 harpocrates-0.1.0.tar.gz
```


---

    Copyright (c) 2018-2020 Privacy1 AB

    THIS REPOSITORY IS THE PROPERTY OF PRIVACY1 AB
    UNAUTHORIZED ACCESS TO THIS DATA IS STRICTLY PROHIBITED.# helm-chart-repo

