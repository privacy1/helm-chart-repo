#!/bin/bash

kubectl scale statefulset --all --replicas=0
kubectl delete statefulset --all

kubectl delete svc harpocrates-mysql-svc
kubectl delete svc cerberus
kubectl delete svc dpia
kubectl delete svc keychain
kubectl delete svc keychain-analytics
kubectl delete svc ldar
kubectl delete svc privacy-console
kubectl delete svc privacyfront

kubectl delete configmap --all

kubectl delete secret sh.helm.release.v1.harpocrates.v1
kubectl delete secret license-secret
kubectl delete secret harpocrates-ssl
kubectl delete secret harpocrates-mysql-secret
kubectl delete secret jwt-secret

kubectl delete pvc harpocrates-mysql-pvc-harpocrates-mysql-0