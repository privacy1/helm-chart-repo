#!/bin/bash

wget https://privacy1.gitlab.io/helm-chart-repo/index.yaml
NUM=$(cat ./index.yaml | shyaml get-length entries.harpocrates)
echo ${NUM}

for (( c=0; c<${NUM}; c++ ))
do
    TGZ=$(cat ./index.yaml | shyaml get-value entries.harpocrates.$c.urls.0)
    wget ${TGZ} -P public
done

NUM=$(cat ./index.yaml | shyaml get-length entries.harpocrates-aws-byol)
echo ${NUM}

for (( c=0; c<${NUM}; c++ ))
do
    TGZ=$(cat ./index.yaml | shyaml get-value entries.harpocrates-aws-byol.$c.urls.0)
    wget ${TGZ} -P public
done

NUM=$(cat ./index.yaml | shyaml get-length entries.harpocrates-aws-paid)
echo ${NUM}

for (( c=0; c<${NUM}; c++ ))
do
    TGZ=$(cat ./index.yaml | shyaml get-value entries.harpocrates-aws-paid.$c.urls.0)
    wget ${TGZ} -P public
done
