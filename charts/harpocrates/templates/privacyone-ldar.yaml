---
apiVersion: v1
kind: Service
metadata:
  name: ldar
  labels:
    app: ldar
spec:
  selector:
    app: ldar
  ports:
    - port: 8091

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: ldar-iam-config
data:
  iam-service-account-key.json: |
    {
    }

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: ldar-config
data:
  ldar.properties: |
    spring.datasource.url=jdbc:mysql://${MYSQL_DB_HOST}:3306/ldardb?createDatabaseIfNotExist=true&serverTimezone=UTC&useLegacyDatetimeCode=false&useSSL=false&allowPublicKeyRetrieval=true
    spring.datasource.username=${MYSQL_DB_USER}
    spring.datasource.password=${MYSQL_DB_PASS}

    # token authorization
    jwt.secret=${JWT_SECRET}

    #
    # Below are optional values for phase II
    #
    server.port = 8091

    # cerberus config
    privacyone.cerberus.host=cerberus
    privacyone.cerberus.port=8093
    privacyone.cerberus.accountPassword=ldar

    # keychain config
    privacyone.keychain.host=keychain
    privacyone.keychain.ldar.serviceKey=<LDAR_SERVICE_KEY>
    privacyone.sdk.keychain.cacheExpiration=300

    # IAM service account for generating temporary archive storage access tokens
    iam.service-account-key.path=/etc/privacyone/ldar/iam-service-account-key.json
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: ldar
  labels:
    app.kubernetes.io/name: ldar
    app: ldar
spec:
  selector:
    matchLabels:
      app: ldar
  replicas: 1
  serviceName: {{ .Release.Name }}-ldar-svc
  template:
    metadata:
      labels:
        app: ldar
    spec:
      containers:
        - name: ldar
          image: {{ .Values.image.ldar }}
          env:
            - name: MYSQL_DB_HOST
              value: {{ .Values.db.host }}
            - name: MYSQL_DB_USER
              value: {{ .Values.db.harpocratesUser }}
            - name: MYSQL_DB_PASS
              value: {{ .Values.db.harpocratesPassword }}
            - name: JWT_SECRET
              valueFrom:
                configMapKeyRef:
                  name: jwt-config
                  key: secret-token
          imagePullPolicy: IfNotPresent
          ports:
            - containerPort: 8091
          volumeMounts:
            - name: ldar-config-volume
              mountPath: /etc/privacyone/ldar/ldar.properties
              subPath: ldar.properties
            - name: ldar-iam-config-volume
              mountPath: /etc/privacyone/ldar/iam-service-account-key.json
              subPath: iam-service-account-key.json
            - name: privacyone-license-volume
              mountPath: /etc/privacyone/license
              subPath: license
            - name: privacyone-messenger-volume
              mountPath: /etc/privacyone/messenger/messenger.properties
              subPath: messenger.properties
      imagePullSecrets:
        - name: privacyone-image-secret
      volumes:
        - name: ldar-config-volume
          configMap:
            name: ldar-config
        - name: ldar-iam-config-volume
          configMap:
            name: ldar-iam-config
        - name: privacyone-license-volume
          configMap:
            name: privacyone-license
        - name: privacyone-messenger-volume
          configMap:
            name: messenger-config
