AWSTemplateFormatVersion: '2010-09-09'
Description: This Cloudformation template use Cloudformation to deploy
  Harpocrates using Helm charts. You will be deploying this into the "Amazon EKS QuickStart"
  which is a qre-requist. "https://docs.aws.amazon.com/quickstart/latest/amazon-eks-architecture/welcome.html" **WARNING** You will be billed for the AWS resources used if you create a stack from this template.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: Kube Cluster
        Parameters:
          - KubeClusterName
          - Name
          - AppVersion
      - Label:
          default: Mysql (RDS)
        Parameters:
          - DBMasterUsername
          - DBMasterUserPassword
          - ServiceAccount
          - TLSKeyNumber
          - TLSKey1Base64
          - TLSKey2Base64
          - TLSKey3Base64
          - TLSCertNumber
          - TLSCert1Base64
          - TLSCert2Base64
          - TLSCert3Base64
          - PrivacyConsoleDomain
          - PrivacyFrontDomain
          - Subnet1ID
          - Subnet2ID
          - VPCID
          - NodeGroupSecurityGroupId
          - SubscriptionType
    ParameterLabels:
      KubeClusterName:
        default: EKS Kube Cluster Name
      Name:
        default: Kube Name for this function
      AppVersion:
        default: Harpocrates application version
      DBMasterUsername:
        default: Mysql Master User Name
      DBMasterUserPassword:
        default: Mysql Master User Password
      ServiceAccount:
        default: ServiceAccount for Harpocrates to use AWS marketplace RegisterUsage API
      TLSKeyNumber:
        default: How many parts of encrypted TLS private key
      TLSKey1Base64:
        default: The first part of encrypted TLS private key for connecting to domain
      TLSKey2Base64:
        default: The second part of encrypted TLS private key for connecting to domain
      TLSKey3Base64:
        default: The third part of encrypted TLS private key for connecting to domain
      TLSCertNumber:
        default: How many parts of encrypted TLS certificate
      TLSCert1Base64:
        default: The first part of encrypted TLS certificate for connecting to domain
      TLSCert2Base64:
        default: The second part of encrypted TLS certificate for connecting to domain
      TLSCert3Base64:
        default: The third part of encrypted TLS certificate for connecting to domain
      PrivacyConsoleDomain:
        default: Domain name of PrivacyConsole
      PrivacyFrontDomain:
        default: Domain name of PrivacyFront
      Subnet1ID:
        default: Private Subnet One
      Subnet2ID:
        default: Private Subnet Two
      VPCID:
        default: EKS Stack VPC ID
      NodeGroupSecurityGroupId:
        default: Node SecurityGroup ID
      SubscriptionType:
        default: Subscription type of harpocrates
Parameters:
   KubeClusterName:
    Description: 'Use the "EKSClusterName" from the EKSStack outputs section in CloudFormation.'
    Type: String
   Name:
    Description: 'Modify to use a custom Names. The Names up to 253 characters
    long. The characters allowed in names are: digits (0-9), lower case letters (a-z), -,
    and ..'
    AllowedPattern: '[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*'
    Type: String
    Default: "privacy1"
   AppVersion:
    Description: 'Select harpocrates application version'
    Type: String
    AllowedValues: ["1.1.0", "1.1.2", "1.5.0"]
   Subnet1ID:
    Description: 'Get Private Subnet 1 ID from the VPCStack outputs section in CloudFormation.'
    Type: AWS::EC2::Subnet::Id
   Subnet2ID:
    Description: 'Get Private Subnet 2 ID from the VPCStack outputs section in CloudFormation.'
    Type: AWS::EC2::Subnet::Id
   NodeGroupSecurityGroupId:
    Description: 'Get NodeGroupSecurityGroupId from the EKSStack outputs section in CloudFormation.'
    Type: String
   VPCID:
    Type: AWS::EC2::VPC::Id
    Description: 'Get VCP ID from the VPCStack outputs section in CloudFormation.'
   DBMasterUsername:
    AllowedPattern: '[a-zA-Z][a-zA-Z0-9]*'
    ConstraintDescription: must begin with a letter and contain only alphanumeric characters.
    Default: p1user
    Description: "The database admin account username"
    MaxLength: '16'
    MinLength: '1'
    Type: String
   DBMasterUserPassword:
    AllowedPattern: '(?=^.{6,255}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'
    ConstraintDescription: "Min 8 chars. Must include 1 uppercase, 1 lowercase, 1 number, 1 (non / @ \" ') symbol"
    Description: "Password for the master account. Password must meeting the following: Min 8 chars. Must include 1 uppercase, 1 lowercase, 1 number, 1 (non / @ \" ') symbol."
    MinLength: 8
    MaxLength: 128
    NoEcho: true
    Type: String
   ServiceAccount:
    Description: "Service account to use AWS marketplace RegisterUsage API. Following https://gitlab.com/privacy1/harpocrates-documentation/-/blob/master/deployment/aws/deployment.md#create-serviceaccount-for-harpocrates-aws-paid"
    Type: String
   TLSKeyNumber:
    Description: "A Base64 encoded TLS key for protecting your Privacy Console and Privacy Front service. The domain name should be consistent with below PrivacyConsoleDomain and PrivacyFrontDomain.
                  Due to AWS cloudfromation string length limitation, a string is not longer then 4096 bytes. So if your encoded TLS key is longer than that, it should be split to several parts.
                  Run command to get this number 'cat YOUR_TLS_KEY_FILE.key | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | wc -w'"
    Type: String
    AllowedValues: ["1", "2", "3"]
   TLSKey1Base64:
    Description: "The first part of encoded TLS key. Run command 'cat YOUR_TLS_KEY_FILE.key | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | awk '{ print $1 }' '"
    Type: String
   TLSKey2Base64:
    Description: "The second part of encoded TLS key if your TLSKeyNumber is bigger than 2. Run command 'cat YOUR_TLS_KEY_FILE.key | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | awk '{ print $2 }' '"
    Default: ""
    Type: String
   TLSKey3Base64:
    Description: "The third part of encoded TLS key if your TLSKeyNumber is 3. Run command 'cat YOUR_TLS_KEY_FILE.key | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | awk '{ print $3 }' '"
    Default: ""
    Type: String
   TLSCertNumber:
    Description: "A Base64 encoded TLS certificate for protecting your Privacy Console and Privacy Front service. The domain name should be consistent with below PrivacyConsoleDomain and PrivacyFrontDomain.
                  Due to AWS cloudfromation string length limitation, a string is not longer then 4096 bytes. So if your encoded TLS certificate is longer than that, it should be split to several parts.
                  Run command to get this number 'cat YOUR_TLS_CRT_FILE.crt | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | wc -w'"
    Type: String
    AllowedValues: ["1", "2", "3"]
   TLSCert1Base64:
    Description: "The first part of encoded TLS certificate. Run command 'cat YOUR_TLS_CRT_FILE.crt | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | awk '{ print $1 }' '"
    Type: String
   TLSCert2Base64:
    Description: "The second part of encoded TLS certificate if your TLSCertNumber is bigger than 2. Run command 'cat YOUR_TLS_CRT_FILE.crt | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | awk '{ print $2 }' '"
    Default: ""
    Type: String
   TLSCert3Base64:
    Description: "The third part of encoded TLS certificate if your TLSCertNumber is 3. Run command 'cat YOUR_TLS_CRT_FILE.crt | base64 -w 0 | sed -r 's/(.{4096})/\\1\\ /g' | awk '{ print $3 }' '"
    Default: ""
    Type: String
   PrivacyConsoleDomain:
    Description: "Choose a domain e.g. console.privacy.YOUR_BIZ_DOMAIN for Privacy Console service which is used by your business."
    Type: String
   PrivacyFrontDomain:
    Description: "Choose a domain e.g. manager.privacy.YOUR_BIZ_DOMAIN for Privacy Front service which is used by your mobile app or web app"
    Type: String
Conditions:
  UsingChartVersion: !Equals [!Ref AppVersion, '1.1.0']
Resources:
  DBEC2SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Open database for access
      VpcId: !Ref VPCID
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 3306
        ToPort: 3306
        SourceSecurityGroupId: !Ref NodeGroupSecurityGroupId
        Description: "This rule is needed to allow RDS from the Node Instances."
  DBSubnetGroup:
    Type: "AWS::RDS::DBSubnetGroup"
    Properties:
      DBSubnetGroupDescription: "Subnets available for the Mysql database instance"
      SubnetIds:
      - !Ref Subnet1ID
      - !Ref Subnet2ID
  DBParameterGroup:
    Type: "AWS::RDS::DBParameterGroup"
    Properties:
      Description: "Parameter groups for harpocrates db. Set sql_mode and log_bin_trust_function_creators"
      Family: "mysql8.0"
      Parameters:
        log_bin_trust_function_creators: 1
        sql_mode: ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
  HARPDB:
    Type: AWS::RDS::DBInstance
    Properties:
      AllocatedStorage: '25'
      AutoMinorVersionUpgrade: true
      BackupRetentionPeriod: 7
      DBInstanceClass: db.m5.large
      # In the next line of code the '-DB' delimeter is used to get the root stack name for database identifier
      # 'AWS::StackName' produces MASTER_STACK_NAME-DB (as DB is the name of the nested stack resource).
      DBInstanceIdentifier: !Sub ["${RootStack}-db", RootStack: !Select [0, !Split ['-DB', !Ref 'AWS::StackName']]]
      DBSubnetGroupName: !Ref DBSubnetGroup
      DBParameterGroupName: !Ref DBParameterGroup
      Engine: mysql
      EngineVersion: '8.0'
      MasterUsername: !Ref DBMasterUsername
      MasterUserPassword: !Ref DBMasterUserPassword
      MultiAZ: false
      StorageEncrypted: true
      StorageType: gp2
      Tags:
        - Key: Name
          Value: !Sub ["${StackName} Confluence Mysql Database", StackName: !Ref 'AWS::StackName']
      VPCSecurityGroups:
      - !GetAtt DBEC2SecurityGroup.GroupId
  HarpHelm:
    Type: AWSQS::Kubernetes::Helm
    Properties:
      ClusterID: !Ref KubeClusterName
      Repository: https://privacy1.gitlab.io/helm-chart-repo
      Chart: 'privacy1/harpocrates-aws-paid'
      Version: !If [UsingChartVersion, '0.2.1', !Ref AppVersion]
      Name: !Ref Name
      Values:
        db.host: !GetAtt HARPDB.Endpoint.Address
        db.harpocratesUser: !Ref DBMasterUsername
        db.harpocratesPassword: !Ref DBMasterUserPassword
        serviceAccount: !Ref ServiceAccount
        tls.base64EncodedPrivateKey: !Sub ${TLSKey1Base64}${TLSKey2Base64}${TLSKey3Base64}
        tls.base64EncodedCertificate: !Sub ${TLSCert1Base64}${TLSCert2Base64}${TLSCert3Base64}
        domain.console: !Ref PrivacyConsoleDomain
        domain.privacyfront: !Ref PrivacyFrontDomain
Outputs:
  HarpHost:
    Value: !GetAtt HARPDB.Endpoint.Address