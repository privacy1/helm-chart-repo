# Harpocrates Deployment

> **Read time**: 15 minutes

> **Prerequisite**: You have gone through pre-deployment and an EKS cluster is up and running

> **Mission**: You manage to deploy Harpocrates in an EKS cluster using the AWS Marketplace

## Overview
Once your have a kubernetes cluster running, it is time to install Harpocrates kubernetes app. 
In this tutorial we walk you, step by step, through the process of using the AWS Marketplace 
to deploy applications on a running EKS cluster. We use Helm chart to deploy the app on your 
EKS cluster.

[Harpocrates Helm chart repo](https://privacy1.gitlab.io/helm-chart-repo) has been setup by PRIVACY1.

## Step by step

### Install helm (minimal version 3.1)
1. Download your [desired version](https://github.com/helm/helm/releases)
2. Unpack it by running 
   
   `$ tar -zxvf helm-v3.2.1-linux-amd64.tar.gz`
3. Find the helm binary in the unpacked directory, and move it to its desired destination e.g.
   
   `$ mv linux-amd64/helm /usr/local/bin/helm`

### Add Harpocrates helm chart repo

```
$ helm repo add privacy1 https://privacy1.gitlab.io/helm-chart-repo
$ helm repo update
```

Running the following command, you should be able to view the available helm chart repo which
includes PRIVACY1 repo you just added.

```
$ helm repo list
```

### Install Harpocreates

Let's create a few input parameters in environment variables so that you can refer them easily in helm install command.

#### Create image pull secret
This only apply for Harpocrates, not for harpocrates-aws-byol and harpocrates-aws-paid.

```
$ kubectl create secret docker-registry privacyone-image-secret \
   --docker-server=https://344250484034.dkr.ecr.us-east-2.amazonaws.com \
   --docker-username=AWS \
   --docker-password="${TOKEN}"
```
#### Set your privacy domain TLS cert and key

The Harpocrates system requires two domains for Privacy Front and Privacy Console services respectively. Privacy Front is
a service to receive all the privacy management requests from your end users. Privacy Console is a portal for different 
roles such as legal, admin, engineering in your business to login and manage data privacy. Read more information from 
[Harpocrates documentation].(https://app.privacyone.co/documentation)

Let's assume your business has a domain called biz.com. It is recommend that you bind a wildcard domain certificate with a 
privacy sub-domain that you own `*.privacy.biz.com`. You can use front.privacy.biz.com and console.privacy.biz.com sub-domains to 
represent Privacy Front and Privacy Console respectively.

Once you have obtained the TLS certificate for your `*.privacy.biz.com` domain, set the base64 encoded TLS key and TLS 
crt to two environment variables using the following commands.


```
$ export TLS_KEY=$(cat <path to your tls key file> | base64 -w 0)
$ export TLS_CRT=$(cat <path to your tls cert file> | base64 -w 0)
```


#### Set Harpocrates license
Obtain Harpocrates license file from [here](https://app.privacyone.co/license/). You can start with a FREE trial license 
or purchase a commercial license. Encode license file using command `cat LICENSE_FILE | base64 -w 0` and set the encoded 
license into an environment variable.

```
$ export LICENSE=$(cat <path to your license file> | base64 -w 0)
```

#### Set database password
There are two users for database. One is root user. another one is Harpocrates user which is be used by Harpocrates
service. You can set your root and normal user password with BASE64 encoded. Also you can specify the Harpocrates user
name.

```
$ export ROOT_PASSWORD=$(echo -n <YOUR_ROOT_PASSWORD> | base64)
$ export HARPOCRATES_PASSWORD=$(echo -n <YOUR_HARPOCRATES_PASSWORD> | base64)
```

#### Create serviceAccount (For harpocrates-aws-paid)

```
$ eksctl utils associate-iam-oidc-provider --cluster <YOUR_CLUSTER_NAME> --approve
$ eksctl create iamserviceaccount --name harpocrates-serviceaccount --cluster <YOUR_CLUSTER_NAME> \
    --attach-policy-arn arn:aws:iam::aws:policy/AWSMarketplaceMeteringRegisterUsage \
    --attach-policy-arn arn:aws:iam::aws:policy/AmazonEC2FullAccess \
    --approve --override-existing-serviceaccounts
```

#### Install Harpocrates using helm

##### Install harpocrates
```
$ helm install harpocrates privacy1/harpocrates \
--set domain.privacyfront=<YOUR_PRIVACY_FRONT_DOMAIN> \
--set domain.console=<YOUR_PRIVACY_CONSOLE_DOMAIN> \
--set tls.base64EncodedPrivateKey=$TLS_KEY \
--set tls.base64EncodedCertificate=$TLS_CRT \
--set p1.license=$LICENSE \
--set db.host=<DB_HOST> \
--set db.harpocratesUser=<DB_HARPOCRATES_USERNAME> \
--set db.harpocratesPassword=$HARPOCRATES_PASSWORD
```


##### Install harpocrates-aws-byol
```
$ helm install harpocrates privacy1/harpocrates-aws-byol \
--set domain.privacyfront=<YOUR_PRIVACY_FRONT_DOMAIN> \
--set domain.console=<YOUR_PRIVACY_CONSOLE_DOMAIN> \
--set tls.base64EncodedPrivateKey=$TLS_KEY \
--set tls.base64EncodedCertificate=$TLS_CRT \
--set p1.license=$LICENSE \
--set db.host=<DB_HOST> \
--set db.harpocratesUser=<DB_HARPOCRATES_USERNAME> \
--set db.harpocratesPassword=$HARPOCRATES_PASSWORD
```

##### Install harpocrates-aws-paid
```
$ helm install harpocrates privacy1/harpocrates-aws-paid \
--set domain.privacyfront=<YOUR_PRIVACY_FRONT_DOMAIN> \
--set domain.console=<YOUR_PRIVACY_CONSOLE_DOMAIN> \
--set tls.base64EncodedPrivateKey=$TLS_KEY \
--set tls.base64EncodedCertificate=$TLS_CRT \
--set serviceAccount=harpocrates-serviceaccount \
--set db.host=<DB_HOST> \
--set db.harpocratesUser=<DB_HARPOCRATES_USERNAME> \
--set db.harpocratesPassword=$HARPOCRATES_PASSWORD \
```

Comments on parameters
 * **domain.privacyfront** Choose a domain e.g. front.privacy.YOUR_BIZ_DOMAIN for Privacy Front service which is used by your mobile app or web app.
 * **domain.console** Choose a domain e.g. console.privacy.YOUR_BIZ_DOMAIN for Privacy Console service which is used by your business.
 * **tls.base64EncodedCertificate=$TLS_CRT** A Base64 encoded TLS key for protecting your Privacy Console and Privacy Front service.  The value has been set into `$TLS_CRT` in the previous step.
 * **tls.base64EncodedPrivateKey**  A Base64 encoded TLS key for protecting your Privacy Console and Privacy Front service.  The value has been set into `$TLS_KEY` in the previous step.
 * **p1.license** Harpocrates license. The value has been set into an env `$LICENSE` in the previous step.
 * **db.rootPassword** Base64 encoded password for database 'root' user.
 * **db.harpocratesUser** Database user name for Harpocrates service account.
 * **db.harpocratesPassword** Base64 encoded password for database Harpocrates service account.
 * **db.storageVolumeSize** Size of disk for MySQL database. We recommend the following sizes according to your choice of Harpocrates license.
   * Lite version 25Gi 
   * Platform Team 50Gi
   * Enterprice 100Gi 
   * Enterprice+ 250Gi
 

### Uninstall Harpocrates
In case you want to remove Harpocrates app, here is the command for you to run with caution. 

> NOTE: Make sure you have backed up all 
> * Harpocrates database 
> * Harpocrates secrets
> * Harpocrate service keys

```
$ helm uninstall harpocrates
```

The above command does not delete the persistence volume `pvc`. If you want to remove it, run the following command with 
caution. 

> NOTE: Once deleted you will lose data in Harpocrates database completely.

```
$ kubectl delete pvc harpocrates-mysql-pvc-harpocrates-mysql-0
```

Delete helm cache if you want to re-install Harpocrates helm chart in the future.

```
$ rm -fr ~/.cache/helm/
```
