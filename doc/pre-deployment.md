# Harpocrates pre-deployment

> **Read time**: 15 minutes

> **Prerequisite**: You have an active [AWS account](https://aws.amazon.com/)

> **Mission**: You have a running EKS cluster

## Install eksctl tool
Follow the instructions [here](https://github.com/weaveworks/eksctl) to install eksctl, a simple CLI tool for creating clusters on EKS.

## Install AWS CLI

You should have awscli installed. Check awscli version by issuing
```
$ aws --version
```

If not, follow the instruction below.

### Linux
Please follow AWS CLI [install instructions](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
to install the AWS command line tool. A quick cheatsheet is listed here.

```
$ pip3 install awscli --upgrade --user
```

Update installed awscli to the latest version
```
$ pip3 install --upgrade --user awscli
```

### Mac
Please follow AWS CLI [Mac install instructions](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html)
to install the AWS command line tool. A quick cheatsheet is listed here.

```
$ curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
$ sudo installer -pkg AWSCLIV2.pkg -target /
```


## Create EKS required policies

Creating an EKS cluster requires a number of AWS permissions in place. Navigate to IAM Management Console-> IAM -> Policies. 

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/aws+marketplace/iam+policy.png" alt="IAM policy" width="400"/>

Click `Create policy` button and Create a policy with properties defined in the Appendix section.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/aws+marketplace/create+json+iam+policy.png" alt="Create plicy" width="700"/>


## Configure EKS required policies

### Existing user
If you'vd already had an existing aws user, you can attach the policy created above to the existing user and skip the 
`New user` section.

1. Open the [IAM console](https://console.aws.amazon.com/iam/)
2. Click Users tab -> the user you are planning to attach the policy we created above
3. Click Add permissions -> Attach existing policies directly and choose the policy you created from the last step
4. Click Review -> Add permissions

### New user
1. Open the [IAM console](https://console.aws.amazon.com/iam/)
2. Click Add user -> Enter User name and check Programmatic access box -> Click Permissions button
3. Click Attach existing policies directly and choose the policy you created from the last step
4. Continue and finish all the necessary steps for creating your user.
5. At the last step you will be prompted an Access key ID and Secret Access key. Save them on your computer safely and 
   will be using the two piece of information in the next step.
6. Run the following command `$ aws configure` and enter the Access key ID and Secret Access key. Also enter the target region.

## Manage your EKS cluster
You can choose to install Harpocrates app either in an existing Kubernetes cluster of yours or create a new cluster. In 
the case of an existing cluster, skip the next step and go to Harpocrates Deployment [tutorial](https://gitlab.com/privacy1/helm-chart-repo/-/blob/master/doc/deployment.md).


### Create RDS database


### Create a cluster
In this step we use a command line tool eksctl to create a new cluster for accommodating Harpocrates app which will be 
deployed in the next [tutorial](https://gitlab.com/privacy1/helm-chart-repo/-/blob/master/doc/deployment.md).

```
$ eksctl create cluster --name harpocrates --region <YOUR_TARGET_REGION> --nodegroup-name harpocrates-nodegroup --node-type t3.medium --nodes 3
```

### Delete a cluster
In case you want to delete a cluster, here is the command for you to run with caution. 

> NOTE: Make sure you have backed up all 
> * Harpocrates database 
> * Harpocrates secrets
> * Harpocrate service keys

```
$ eksctl delete cluster --name harpocrates --region <YOUR_TARGET_REGION>
```

## Appendix

### IAM policy

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ec2:DeleteInternetGateway",
            "Resource": "arn:aws:ec2:::internet-gateway/"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "eks:DescribeFargateProfile",
                "ec2:AttachInternetGateway",
                "iam:PutRolePolicy",
                "iam:AddRoleToInstanceProfile",
                "ec2:DeleteRouteTable",
                "ec2:CreateRoute",
                "ec2:CreateInternetGateway",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:DeleteInternetGateway",
                "ec2:DescribeKeyPairs",
                "iam:GetRole",
                "ec2:ImportKeyPair",
                "ec2:CreateTags",
                "ecr:GetAuthorizationToken",
                "ec2:RunInstances",
                "iam:DeleteRole",
                "ec2:DisassociateRouteTable",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:DescribeImageAttribute",
                "ec2:DeleteNatGateway",
                "eks:UpdateNodegroupConfig",
                "autoscaling:DeleteAutoScalingGroup",
                "eks:ListClusters",
                "ec2:CreateSubnet",
                "ec2:DescribeSubnets",
                "iam:GetOpenIDConnectProvider",
                "iam:GetRolePolicy",
                "iam:CreateInstanceProfile",
                "ec2:CreateNatGateway",
                "ec2:CreateVpc",
                "ec2:DescribeVpcAttribute",
                "ec2:ModifySubnetAttribute",
                "iam:ListInstanceProfilesForRole",
                "iam:PassRole",
                "ec2:DescribeAvailabilityZones",
                "autoscaling:DescribeScalingActivities",
                "iam:DeleteRolePolicy",
                "eks:CreateCluster",
                "ec2:ReleaseAddress",
                "iam:DeleteInstanceProfile",
                "ec2:DeleteLaunchTemplate",
                "eks:UntagResource",
                "ec2:DescribeSecurityGroups",
                "autoscaling:CreateLaunchConfiguration",
                "ec2:CreateLaunchTemplate",
                "iam:CreateServiceLinkedRole",
                "ec2:DescribeVpcs",
                "eks:TagResource",
                "ec2:DeleteSubnet",
                "eks:ListTagsForResource",
                "iam:RemoveRoleFromInstanceProfile",
                "iam:CreateRole",
                "eks:UpdateClusterConfig",
                "iam:AttachRolePolicy",
                "ssm:GetParameter",
                "ec2:AssociateRouteTable",
                "ec2:DescribeInternetGateways",
                "eks:DescribeNodegroup",
                "iam:DetachRolePolicy",
                "ssm:DescribeParameters",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:UpdateAutoScalingGroup",
                "ec2:DescribeRouteTables",
                "eks:ListNodegroups",
                "ec2:DescribeLaunchTemplates",
                "ec2:CreateRouteTable",
                "cloudformation:*",
                "ssm:GetParameters",
                "ec2:DetachInternetGateway",
                "eks:DeleteCluster",
                "eks:DeleteNodegroup",
                "eks:DescribeCluster",
                "iam:DeleteServiceLinkedRole",
                "ec2:DeleteVpc",
                "autoscaling:CreateAutoScalingGroup",
                "eks:UpdateClusterVersion",
                "ec2:DescribeAddresses",
                "ec2:DeleteTags",
                "autoscaling:DescribeLaunchConfigurations",
                "eks:UpdateNodegroupVersion",
                "ec2:CreateSecurityGroup",
                "eks:ListUpdates",
                "ec2:ModifyVpcAttribute",
                "ec2:AuthorizeSecurityGroupEgress",
                "iam:GetInstanceProfile",
                "ec2:DescribeTags",
                "ec2:DeleteRoute",
                "ec2:DescribeLaunchTemplateVersions",
                "ec2:DescribeNatGateways",
                "eks:CreateNodegroup",
                "iam:ListInstanceProfiles",
                "ec2:AllocateAddress",
                "ec2:DescribeImages",
                "eks:ListFargateProfiles",
                "autoscaling:DeleteLaunchConfiguration",
                "eks:DescribeUpdate",
                "ec2:DeleteSecurityGroup"
            ]
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": "ssm:GetParameter",
            "Resource": "arn:aws:ssm:::parameter/"
        }
    ]
}
```
